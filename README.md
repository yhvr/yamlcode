# YAMLCode

YAML, but it's a turing-complete programming language.

<hr />

## Installation

```sh
# If you just wanna install it
npm install -g yamlcode
# If you're feeling fancy
git clone https://gitlab.com/yhvr/yamlcode.git && cd yamlcode
npm install
npm link
```

## CLI

See `yamlcode` for usage. In case you can't, here it is.

```
yamlcode [file] <options>

  [file] ............ Run the .yml file [file].
  -h, --help ........ View this menu.
  -v, --version ..... Get the version of yamlcode.
```

## Usage

> Just looking for a template? Check the `examples` folder!

> Looking for advanced usage? See the [Online Docs](https://youtu.be/umDr0mPuyQc).

yamlcode isn't very big, so here is documentation on everything in it. Written in YAML... haha...

### Using Functions
```yml
# if a function has one arg, you can just do this.
- log: 1
# if it has multiple, you'll need to split it up a little.
- log:
    - add:
        - 0.5
        - 0.5
```

### Functions

```yml
# Keyv storage
set: "(2 args, a b) Set value `a` to `b`."
get: "(1 arg, a) Get value `a`."
# Misc.
log: "(1 arg, a) Log `a` to the console."
# Math
add: "(2 args, a b) Return `a + b`."
sub: "(2 args, a b) Return `a - b`."
mul: "(2 args, a b) Return `a * b`."
div: "(2 args, a b) Return `a / b`."
mod: "(2 args, a b) Return `a mod b`."
# Comparison
eq: "(2 args, a b) Return `a === b`."
neq: "(2 args, a b) Return `a !== b`."
leq: "(2 args, a b) Return `a == b`. Type doesn't matter."
lneq: "(2 args, a b) Return `a != b`. Type doesn't matter."
and: "(2 args, a b) Return `a && b`."
or: "(2 args, a b) Return `a || b`."
not: "(1 arg, a) Returns the opposite of `a`."
```

### Keywords

```yml
# func: delcare a function

- func:
  name: helloWorld
  commands:
    - log: "Hello, world!"
# run the function
- exec: helloWorld


# repeat: repeat something

# n times. starts at 0.
- repeat:
  times: 5
  # var is optional. it
  # specifies a variable
  # that you can `get`
  # during the loop
  var: i
  commands:
    - log:
        - get: i


# if: evaluate a variable.

- set:
    - var
    - true
- if:
  condition: var
  then:
    - log: "True!"
  # else is optional.
  else:
    - log: "False!"


# while: a while loop, what do you expect?

- set:
    - var
    - true
- while:
  condition: var
  commands:
    - log: "console spam!"


# import: include a JS file in your YAMLCode
# IMPORTANT: YAMLCode takes the file from the
# folder you ran the command in, not where the file came from.
- import:
  from: foo.js
  # `as` is optional.
  as: sayHi
- exec: sayHi
# example foo.js:
# module.exports = {
# default: "sayHello",
# version: 1,
# func: () => console.log("Greetings!"),
# };


# include: Make your YAMLCode modular!

# index.yml
- include:
  from: foo.yml
- log:
   - get: a
# foo.yml
- set:
  - a
  - 5
```

If you have any bugs or feature requests, feel free to email me at `yhvr@pm.me`.