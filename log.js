"use strict";

const chalk = require("chalk");

module.exports = {
	error: x => {
		console.log(chalk.redBright(x));
		process.exit(1);
	},
	warn: x => {
		console.log(chalk.yellowBright(x));
	}
};