"use strict";
/* eslint-disable no-console, no-unused-vars */

const moduleVersion = 1;
const { NodeVM } = require("vm2");
const log = require("./log.js");
const YAML = require("yaml");
const fs = require("fs");

const env = {
	exec: (_, a) => {
		try {
			return env[a]();
		} catch (e) {
			log.error(`${a} is not a function`);
		}
		return 0;
	},
	log: (_, a) => console.log(a),
	add: (_, a, b) => a + b,
	sub: (_, a, b) => a - b,
	mul: (_, a, b) => a * b,
	div: (_, a, b) => a / b,
	mod: (_, a, b) => a % b,
	set: (_, a, b) => (env[a] = b),
	get: (_, a) => env[a],
	neq: (_, a, b) => a !== b,
	eq: (_, a, b) => a === b,
	lneq: (_, a, b) => a != b,
	leq: (_, a, b) => a == b,
	not: (_, a) => !a,
	and: (_, a, b) => a && b,
	or: (_, a, b) => a || b,
};

function runFunction(expr, args, command) {
	const evaluated = (args instanceof Array ? args : [args]).map(n => {
		if (n instanceof Object && !(n instanceof Array)) {
			if (Object.keys(n).length === 1) return evaluate(n);
			return n;
		}
		return n;
	});
	const out = env[command](expr, ...evaluated);
	return out;
}

function repeat(expr) {
	let times = expr.times;
	// Typecast times as a number in case it isn't already
	if (typeof times === "string") times = Number(env[times]);
	// If they've specified a variable to bind it to.
	if (expr.var) {
		for (env[expr.var] = 0; env[expr.var] < times; env[expr.var]++)
			expr.commands.forEach(evaluate);
	} else {
		// If not, we can just use let.
		for (let i = 0; i < times; i++) expr.commands.forEach(evaluate);
	}
}

function ifStatement(expr) {
	let condition = expr.condition;
	condition = env[condition];
	if (condition) {
		expr.then.forEach(evaluate);
	} else if (expr.else) {
		expr.else.forEach(evaluate);
	}
}

function importStatement(expr) {
	const content = fs.readFileSync(expr.from).toString();
	// Initialize the VM
	const vm = new NodeVM({
		console: "inherit",
		sandbox: {},
		require: {
			external: true,
			builtin: ["fs", "path"],
			root: "./",
		},
	});
	// Run the code
	const output = vm.run(content);
	if (!output)
		log.error(`A dependency (${expr.from}) lacked module.exports.`);
	if (!output.default && !expr.as)
		log.error(
			`Dependency ${expr.from} did not specify a default export name and you didn't specify one.`
		);
	if (output.version !== moduleVersion)
		log.error(
			`Dependency ${expr.from} is either too young or too old. (Expected version ${moduleVersion}, got ${output.version})`
		);
	env[expr.as || output.default] = output.func;
}

function include(expr) {
	let content;
	try {
		content = fs.readFileSync(expr.from);
	} catch (e) {
		log.error("It doesn't look like that file exists.");
	}
	// "best practices" my ass!
	module.exports(content.toString());
}

function evaluate(expr) {
	// It's not an object, so let's just spit it back out.
	if (
		typeof expr === "number" ||
		typeof expr === "string" ||
		typeof expr === "boolean"
	)
		return expr;

	const command = Object.keys(expr)[0];
	const args = expr[command];
	// It has arguments, so it must be a function. Let's run it!
	if (args) return runFunction(expr, args, command);
	// It doesn't have arguments, must be a keyword.
	switch (null) {
		case expr.func:
			env[expr.name] = () => {
				expr.commands.forEach(evaluate);
			};
			break;
		case expr.repeat:
			repeat(expr);
			break;
		case expr.if: {
			ifStatement(expr);
			break;
		}
		case expr.while:
			while (env[expr.condition]) expr.commands.forEach(evaluate);
			break;
		case expr.import:
			importStatement(expr);
			break;
		case expr.include:
			include(expr);
			break;
		default:
			log.error(
				`It looks like you tried to call a function but it doesn't exist.
==========
At function ${Object.keys(expr)[0]}`
			);
	}
	return false;
}

module.exports = function parse(text) {
	let yaml;
	try {
		yaml = YAML.parse(text);
	} catch (e) {
		log.error("Your YAML is invalid!");
	}
	yaml.forEach(expr => evaluate(expr));
};
