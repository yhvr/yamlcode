#!/usr/bin/env node
/* eslint-disable no-console */
"use strict";

// YAMLCode by Yhvr.
// This is my realm.
// I can do what I want here.

const parse = require("./index.js");
const minimist = require("minimist");
const args = minimist(process.argv.slice(2));
const chalk = require("chalk");

switch (true) {
	case args._.length > 0:
		parse(require("fs").readFileSync(args._.join(" ")).toString());
		break;
	case args.v || args.version:
		console.log(require("./package.json").version);
		break;
	case args.h || args.help || (args._[0] === undefined):
		console.log(`${chalk.greenBright("yamlcode [file] <options>")}

  ${chalk.blueBright("[file]")} ............ Run the .yml file [file].
  ${chalk.blueBright("-h, --help")} ........ View this menu.
  ${chalk.blueBright("-v, --version")} ..... Get the version of YAMLCode.`);
		break;
}
